#define _GNU_SOURCE
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define APPROX 10000000
#define PRINT_POINT(pt) printf("[%f,%f]", (double)pt.x / APPROX, (double)pt.y / APPROX)

typedef struct {
  long long x;
  long long y;
} pair_t;

typedef struct {
  pair_t *pair;
  size_t len;
} pairs_t;

typedef struct {
  long long x;
  long long y;
  pair_t *center;
  double d;
} pt_t;

typedef struct {
  pt_t *pt;
  size_t len;
} pts_t;

void __exit(char const* msg) {
  perror(msg);
  exit(1);
}

void display(pairs_t *centers, pts_t *pts) {
  int bool = 0;
  printf("[");
  for (unsigned int i = 0; i < centers->len; ++i) {
    if (i > 0)
      printf(",");
    printf("[");
    PRINT_POINT(centers->pair[i]);
    printf(",[");
    bool = 0;
    for (unsigned int j = 0; j < pts->len; ++j) {
      if (pts->pt[j].center == &centers->pair[i]) {
	if (bool)
	  printf(",");
	PRINT_POINT(pts->pt[j]);
	bool = 1;
      }
    }
    printf("]]");
  }
  printf("]\n");
}

void calculate(pairs_t *centers, pts_t *pts) {
  pairs_t centers_tmp;

  centers_tmp.len = centers->len;
  if ((centers_tmp.pair = malloc(sizeof(*centers_tmp.pair) * centers->len)) == NULL)
    __exit("malloc");
  do {
    memcpy(centers_tmp.pair, centers->pair, sizeof(*centers_tmp.pair) * centers->len);
    for (unsigned int i = 0; i < pts->len; ++i) {
      pt_t *cur = &pts->pt[i];
      pair_t *center_ref = NULL;
      double d = 0xffffffffffffffff;
      for (unsigned int j = 0; j < centers->len; ++j) {
	pair_t *center = &centers->pair[j];
	double tmp_d = sqrt(pow(cur->x - center->x, 2) + pow(cur->y - center->y, 2));
	if (tmp_d < d) {
	  center_ref = center;
	  d = tmp_d;
	}
      }
      cur->center = center_ref;
    }

    for (unsigned int i = 0; i < centers->len; ++i) {
      if (centers->len == 0)
	continue;
      long long x = 0, y = 0;
      unsigned int k = 0;
      for (unsigned int j = 0; j < pts->len; ++j) {
	if (pts->pt[j].center == &centers->pair[i]) {
	  x += pts->pt[j].x;
	  y += pts->pt[j].y;
	  ++k;
	}
      }
      if (k) {
	centers->pair[i].x = x / k;
	centers->pair[i].y = y / k;
      }
    }
  } while (memcmp(centers_tmp.pair, centers->pair, sizeof(*centers_tmp.pair) * centers->len) != 0);
  display(centers, pts);
}

int main(int argc, char **argv) {
  FILE *file;
  char *line = NULL;
  size_t n = 0;
  pts_t pts;
  pairs_t centers;
  ssize_t r;
  int k = 0;

  pts.len = 0;
  centers.len = (argc - 2) / 2;
  pts.pt = NULL;
  if ((file = fopen(argv[1], "r")) == NULL)
    __exit("file");
  if ((centers.pair = malloc(((argc - 2) / 2) * sizeof(*centers.pair))) == NULL)
    __exit("malloc");
  for (int i = 2, j = 0; i < argc; i += 2, ++j) {
    centers.pair[j].x = atof(argv[i]) * APPROX;
    centers.pair[j].y = atof(argv[i + 1]) * APPROX;
  }
  while ((r = getline(&line, &n, file)) != -1) {
    if (r < 2) {
      line = NULL;
      continue;
    }
    if ((pts.pt = realloc(pts.pt, (k + 1) * sizeof(*pts.pt))) == NULL)
      __exit("realloc");
    pts.pt[k].x = atof(line) * APPROX;
    pts.pt[k].y = atof(strchr(line, ';') + 1) * APPROX;
    ++pts.len;
    free(line);
    line = NULL;
    ++k;
  }
  calculate(&centers, &pts);
  free(pts.pt);
  free(centers.pair);
  fclose(file);
  return 0;
}
