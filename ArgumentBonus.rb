require 'optparse'

class ParseError < StandardError; end
class Argument

  CITATION = "L'important c'est les valeurs. -- Perceval."
  attr_reader :file, :points, :format, :langage
  def initialize
    @opts = OptionParser.new do |opts|
      opts.banner = "Usage: ./discriminator ...".ljust(80 - CITATION.size, ' ') + CITATION

      opts.on("--file=FILE", "-f=FILE", "File to read") do |file|
        @file = file
      end

      opts.on("--stdin") do
        @file = 1
      end

      opts.on("--dimensions=D", "-d=D", "Number of dimensions") do |d|
        @d = Integer(d)
        raise ArgumentError, "Must have at least 1 dimensions" if @d < 1
      end

      opts.on("--format=[json,xml,plain]") do |format|
        @format = format
      end
    end.parse!

    @d ||= 2
    @file ||= @opts[0]
    @format ||= "plain"
    @langage = (@d == 2 ? "c" : "ruby")

    raise ArgumentError, "File required" unless @file
    if @file.is_a? Integer
      @fd = File.open(@file)
    else
      raise ArgumentError, "File does'nt exist" unless File.exists? @file
      @fd = File.open(@file, "r")
    end
    l = 0
    @points = @fd.read.split("\n").map do |line|
      l += 1
      if line.empty?
        nil
      else
        val = '([\-\+]?\d+(\.\d+)?)'
        raise ParseError, "Invalid file of points (l:#{l})" unless line.match(/#{val}(;#{val}){#{@d-1}}/)
        Vector[*line.split(';').map(&:to_f)]
      end
    end.compact
    raise ParseError, "Empty file" if @points.empty?
  end

end
