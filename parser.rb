require 'matrix'

class Matrix
  def show round=0, out=STDOUT
    size = self.map{|e| e.round(round).to_s.size}.max
    self.each_slice(self.column_count){|tab| out.puts tab.map{|e| e.round(round).to_s.rjust(size, ' ')}.join(' ')}
  end
end
