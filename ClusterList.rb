#require_relative 'clusterlistext'
require 'json'

class ClusterList

  #include ClusterListExt

  attr_reader :k, :points
  def initialize k, points, langage="c"
    self.k = k
    self.points = points
    @langage = langage
  end

  def k= k
    @k = Integer(k)
    @clusers = nil
  end

  def points= points
    @points = points.dup
    @clusers = nil
  end

  def kmeanpp
    centers = [@points.first]
    centers_id = [0]
    (@k-1).times do |k|
      i_point = -1
      p = @points.map do |point|
        i_point += 1
        i_center = -1
        c = centers.map do |center|
          i_center += 1
          d = (point - center).magnitude
          {d: d, i_center: i_center}
        end
        {centers: c, i_point: i_point}
      end
      p.delete_if do |e|
        centers_id.include? e[:i_point]
      end
      idx = p.sort_by{|e| e[:centers].sort_by{|f| f[:d]}[-1][:d] }[-1][:i_point]
      centers << @points[idx]
      centers_id << idx
    end
    # point, list
    centers.map{|point| [point, []]}
  end

  def clusters
    return @clusters if @clusters

    clusters = kmeanpp

    if @langage == "c"
      clusters_send = clusters.map{|c| c[0].to_a.join(" ")}.join(" ")
      r = `./cluster_list_loop #{ARGV[0]} #{clusters_send}`.to_s.chomp
      # errors there
      begin
        clusters = JSON.parse(r)
      rescue
        raise RuntimeError, r
      end
    elsif @langage == "ruby"
      loop do
        # deep copy of the points
        clusters_cpy = clusters.map{|e| e[0]}
        points.each do |point|
          idx = nil
          d = Float::INFINITY
          # computes Dn(point) (distance)
          clusters.each_with_index do |cluster, i|
            tmp_d = (cluster[0] - point).magnitude
            if tmp_d < d
              idx = i
              d = tmp_d
            end
          end
          # associate the point with the nearest cluster
          clusters[idx][1] << point
        end

        # c = [point, list]
        clusters.each_with_index do |c, idx|
          c[0] = c[1].inject(&:+) / c[1].size.to_f # recentrer
          clusters[idx][2] = c[1] # save old list
          clusters[idx][1] = [] # reset the list
        end

        break if clusters.map{|e| e[0]} == clusters_cpy
      end
    end

    @clusters = clusters
    @clusters.map!{|e| {center: Vector[*e[0]], points: e[1].map{|p| Vector[*p]} } }
    @clusters.freeze
  end

  def rss_details
    self.clusters.map do |c|
      c[:points].C_RSS(c[:center])
    end
  end

  def rss
    self.clusters.reduce(0.0) do |total, c|
      total + c[:points].C_RSS(c[:center])
    end
  end

end
