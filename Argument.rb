require 'optparse'
require 'pry' if ENV["DEBUG"] == "true"

class ParseError < StandardError; end
class Argument

  attr_reader :file, :points, :format
  def initialize
    raise ArgumentError, "Too many parameters" unless ARGV.size == 1

    @format = "plain"
    @file = ARGV[0]
    raise ArgumentError, "File doesn't exist" unless File.exists? @file
    @fd = File.open(@file, "r")

    l = 0
    @points = @fd.read.split("\n").map do |line|
      l += 1
      if line.empty?
        nil
      else
        val = '([\-\+]?\d+(\.\d+)?)'
        raise ParseError, "Invalid file of points (l:#{l})" unless line.match(/#{val}(;#{val})/)
        Vector[*line.split(';').map(&:to_f)]
      end
    end.compact
    if @points.empty?
      STDOUT.puts 0
      exit 0
    end
  end

end
