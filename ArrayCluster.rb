module ArrayCluster

  def C_valid?
    self.reduce{|r,l| r && l.is_a?(Vector) }
  end

  def C_RSS(center)
    self.reduce(0.0) do |total, point|
      total + (point - center).magnitude ** 2
    end
  end

end

class Array
  prepend ArrayCluster
end
